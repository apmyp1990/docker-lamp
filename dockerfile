FROM ubuntu:18.04

MAINTAINER Artur Schwenke

LABEL Description="Lamp stack with Ubuntu 18.04"

RUN apt update
RUN apt upgrade -y

RUN echo "Europe/Berlin" > /etc/localtime

RUN apt install -y zip unzip wget
RUN apt install -y \
	php \
	php-bz2 \
	php-cgi \
	php-cli \
	php-common \
	php-curl \
	php-dev \
	php-enchant \
	php-fpm \
	php-gd \
	php-gmp \
	php-imap \
	php-interbase \
	php-intl \
	php-json \
	php-ldap \
	php-mbstring \
	php-mysql \
	php-odbc \
	php-opcache \
	php-pgsql \
	php-phpdbg \
	php-pspell \
	php-readline \
	php-recode \
	php-snmp \
	php-sqlite3 \
	php-sybase \
	php-tidy \
	php-xmlrpc \
	php-xsl \
	php-zip
RUN apt install apache2 libapache2-mod-php -y
RUN apt install mariadb-common mariadb-server mariadb-client -y
RUN apt install default-jre -y
RUN apt install nodejs npm sudo curl ftp -y

#RUN apt install firefox -y

# Install Chrome
#RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
#RUN dpkg -i google-chrome-stable_current_amd64.deb; apt-get -fy install

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php composer-setup.php --install-dir=/usr/bin --filename=composer
RUN php -r "unlink('composer-setup.php');"

RUN npm install -g nightwatch

ENV TERM dumb 

RUN service mysql start

RUN a2enmod rewrite
#RUN ln -s /usr/bin/nodejs /usr/bin/node
RUN chown -R www-data:www-data /var/www/html

VOLUME /var/www/html
VOLUME /var/log/httpd
VOLUME /var/lib/mysql
VOLUME /var/log/mysql
VOLUME /etc/apache2

EXPOSE 80 
EXPOSE 3306 
